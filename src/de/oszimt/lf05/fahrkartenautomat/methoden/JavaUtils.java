package de.oszimt.lf05.fahrkartenautomat.methoden;

import java.util.Scanner;

public class JavaUtils {

	private JavaUtils() {
		throw new IllegalStateException("Utility class");
	}

	// Methode zum Erfassen der Werte
	public static void fahrkartenbestellungErfassen(Scanner tastatur) {

		// Festlegen der Variablen
		double ticketpreis;
		int ticketanzahl = 0;

		// Der Scanner "tastatur" wartet auf die n�chste Eingabe eines Integers
		System.out.printf("Anzahl der Tickets: ");
		ticketanzahl = tastatur.nextInt();

		// Der Scanner "tastatur" wartet auf die n�chste Eingabe einer Dezimalzahl
		System.out.printf("Zu zahlender Betrag (EURO): ");
		ticketpreis = tastatur.nextDouble();

		fahrkartenBezahlen(ticketanzahl, ticketpreis, tastatur);
	}

	// Methode zum Berechnen des Preises
	public static void fahrkartenBezahlen(int ticketanzahl, double ticketpreis, Scanner tastatur) {

		// Festlegen der Variablen
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		double rueckgabebetrag;

		// Hier wird der Wert f�r den gesamten zu Zahlenden Betrag aus dem Ticketpreis
		// und der Ticketanzahl errechnet
		zuZahlenderBetrag = ticketanzahl * ticketpreis;

		// Geldeinwurf
		eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double euro = zuZahlenderBetrag - eingezahlterGesamtbetrag;

			System.out.printf("Noch zu Zahlen: %.2f", euro).print(" Euro \n");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		// Fahrscheinausgabe
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

		System.out.println("\n\n");

		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		rueckgeldAusgeben(rueckgabebetrag);
	}

	// Methode zum Berechnen des R�ckgeldbetrags
	public static void rueckgeldAusgeben(double rueckgabebetrag) {
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der rueckgabebetrag in H�he von %.2f", rueckgabebetrag).print(" Euro \n");

			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05) // 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	public static void runAllMethods(Scanner tastatur) {
		fahrkartenbestellungErfassen(tastatur);
	}
}