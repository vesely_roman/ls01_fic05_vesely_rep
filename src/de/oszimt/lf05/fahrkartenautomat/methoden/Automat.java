package de.oszimt.lf05.fahrkartenautomat.methoden;

import java.util.Scanner;

class Automat {
	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		JavaUtils.runAllMethods(tastatur);
	}
}