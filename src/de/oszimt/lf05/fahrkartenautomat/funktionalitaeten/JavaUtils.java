package de.oszimt.lf05.fahrkartenautomat.funktionalitaeten;

import java.text.DecimalFormat;
import java.util.Scanner;

public class JavaUtils {

	private Scanner input = new Scanner(System.in);
	private DecimalFormat df = new DecimalFormat("0.00");

	/*
	 * Methode zur Fahrkartenbestellung. Der Benutzer soll beim Kauf einer Fahrkarte
	 * zwischen verschiedenen Arten von Fahrkarten, siehe Beispiel unten, w�hlen
	 * k�nnen. Der Automat soll dann abh�ngig von der Entscheidung des K�ufers den
	 * richtigen Fahrkartenpreis ermitteln und den Gesamtpreis berechnen. Es gelten
	 * die aktuellen VBB-Preise. Dabei soll immer nur eine Fahrkartenart gekauft
	 * werden
	 */

	public void fahrkartenbestellungErfassen() {
		int selection;
		double zuZahlenderBetrag = 0.0;
		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("\nW�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
		System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		selection = input.nextInt();

		if (selection <= 3 && selection > 0) {
			if (selection == 1) {
				zuZahlenderBetrag = 2.9;
			}
			if (selection == 2) {
				zuZahlenderBetrag = 8.6;
			}
			if (selection == 3) {
				zuZahlenderBetrag = 23.5;
			}
		} else {
			System.out.println("\nIhre Wahl: " + selection);
			System.out.println(">>falsche Eingabe<<\n");
			fahrkartenbestellungErfassen();
		}

		System.out.println("\nIhre Wahl: " + selection);
		System.out.println("Anzahl der Tickets:");
		int amount = 1;
		amount = input.nextInt();
		zuZahlenderBetrag = zuZahlenderBetrag * amount;
		fahrkartenBezahlen(zuZahlenderBetrag);
	}

	// Methode zum Bezahlvorgang
	public void fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("\nNoch zu zahlen: " + df.format(zuZahlenderBetrag - eingezahlterGesamtbetrag) + " �");
			System.out.print("Eingabe (mind. 5 Ct, h�chstens 2 �): ");
			Scanner input = new Scanner(System.in);
			double eingeworfeneM�nze = input.nextDouble();
			if (eingeworfeneM�nze == 2.0 || eingeworfeneM�nze == 1.0 || eingeworfeneM�nze == 0.5
					|| eingeworfeneM�nze == 0.2 || eingeworfeneM�nze == 0.1 || eingeworfeneM�nze == 0.05) {
				eingezahlterGesamtbetrag += eingeworfeneM�nze;
			} else {
				System.out.print("Nur M�nzen!\n");
				fahrkartenBezahlen(zuZahlenderBetrag - eingezahlterGesamtbetrag);
			}
		}
		fahrkartenAusgeben();
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}

	// Methode zur Fahrkartenausgabe
	public void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int index = 0; index < 8; index++) {
			System.out.print("=");
			wait(100);
		}
		System.out.println("\n\n");
	}

	public void wait(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Methode zur Muenzausgabe
	public void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	// Methode zur Rueckgeldausgabe
	public void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		int change = (int) eingezahlterGesamtbetrag * 100;
		change = change - ((int) (zuZahlenderBetrag * 100));
		if (change > 0.0) {
			System.out.println("\nR�ckgabebetrag in H�he von " + df.format((double) change / 100)
					+ " � wird in folgenden M�nzen gezahlt:");

			while (change >= 200.0) // 2 �-M�nzen
			{
				muenzeAusgeben(2, "�");
				change -= 200.0;
			}
			while (change >= 100.0) // 1 �-M�nzen
			{
				muenzeAusgeben(1, "�");
				change -= 100.0;
			}
			while (change >= 50.0) // 50 Ct-M�nzen
			{
				muenzeAusgeben(50, "Ct");
				change -= 50.0;
			}
			while (change >= 20.0) // 20 Ct-M�nzen
			{
				muenzeAusgeben(20, "Ct");
				change -= 20.0;
			}
			while (change >= 10.0) // 10 Ct-M�nzen
			{
				muenzeAusgeben(50, "Ct");
				change -= 10.0;
			}
			while (change >= 5.0)// 5 Ct-M�nzen
			{
				muenzeAusgeben(5, "Ct");
				change -= 5.0;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
		System.out.println();

		// Der Fahrkartenautomat soll nach Abwicklung eines Kaufvorgangs sofort f�r
		// weitere Kaufvorg�nge bereitstehen
		fahrkartenbestellungErfassen();
	}
}
