package de.oszimt.lf05.fahrkartenautomat;

import java.util.Scanner;

public class Automat {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		// Festlegen der Variablen. Dabei sind "double" Dezimalzahlen und "int" ganze
		// Zahlen.
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		double r�ckgabebetrag;
		double preis;
		int ticketanzahl;

		// Der Scanner "tastatur" wartet auf die n�chste Eingabe eines Integers.
		System.out.printf("Anzahl der Tickets: ");
		ticketanzahl = tastatur.nextInt();

		// Der Scanner "tastatur" wartet auf die n�chste Eingabe einer Dezimalzahl.
		System.out.printf("Zu zahlender Betrag (EURO): ");
		preis = tastatur.nextDouble();

		// Hier wird der Wert f�r den gesamten zu Zahlenden Betrag aus dem Ticketpreis
		// und der Ticketanzahl errechnet (Multiplikation).
		zuZahlenderBetrag = ticketanzahl * preis;

		// Geldeinwurf. In Zeile 29 ist mit folgendem Befehl festgelegt, dass der
		// eingezahlte Gesamtbetrag mit 2 Nachkommastellen angezeigt wird: "%.2f"
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: " + "%.2f" + " Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.printf("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		// Fahrscheinausgabe. In Zeile 40 wurde der Wert von sleep auf 100 ms reduziert,
		// um den Fahrschein schneller drucken zu lassen.
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
			}
		}
		System.out.println("\n\n");

		// R�ckgeldberechnung und -Ausgabe
		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f", r�ckgabebetrag).print(" EURO ");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05) // 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

		tastatur.close();
	}
}