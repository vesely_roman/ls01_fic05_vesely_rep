package de.oszimt.lf05.konsolenausgabe;

import java.util.Scanner;

public class EVA {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		int zahl1;
		int zahl2;
		int ergebnis;

		// Eingabe:
		// =======
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		zahl1 = tastatur.nextInt();
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		zahl2 = tastatur.nextInt();

		tastatur.close();

		// Verarbeitung:
		// ============
		ergebnis = addiere(zahl1, zahl2);

		// Ausgabe:
		// =======
		System.out.print("Das Ergebnis der Addition lautet: \n");
		System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
	}

	// Eingabeparameter der Funktion addiere
	// sind zwei ganze Zahlen a und b
	public static int addiere(int a, int b) {
		// Die Funktion berechnet die Summe
		// (verarbeitet die Eingabeparameter)...
		int summe = a + b;
		// ...und gibt die Summe als Ergebniswert zur�ck
		return summe;

	}
}