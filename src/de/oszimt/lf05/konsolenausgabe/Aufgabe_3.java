package de.oszimt.lf05.konsolenausgabe;

public class Aufgabe_3 {

	public static void main(String[] args) {

		String l = "|";
		String f = "Fahrenheit";
		String c = "Celsius";

		int fa = -20;
		int fb = -10;
		int fc = 0;
		int fd = +20;
		int fe = +30;

		double ca = -28.8889;
		double cb = -23.3333;
		double cc = -17.7778;
		double cd = -6.6667;
		double ce = -1.1111;

		System.out.printf("%-12s|%10s%n", f, c);
		System.out.printf("------------------------%n");
		System.out.printf("%s %9s %9.2f %n", fa, l, ca);
		System.out.printf("%s %9s %9.2f %n", fb, l, cb);
		System.out.printf("%s %11s %9.2f %n", fc, l, cc);
		System.out.printf("+%s %9s %9.2f %n", fd, l, cd);
		System.out.printf("+%s %9s %9.2f %n", fe, l, ce);

	}
}
