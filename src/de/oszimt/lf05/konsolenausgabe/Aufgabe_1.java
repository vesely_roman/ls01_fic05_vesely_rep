package de.oszimt.lf05.konsolenausgabe;

public class Aufgabe_1 {

	public static void main(String[] args) {

		String a = "**";
		String b = "*";

		System.out.printf("%5s \n", a);
		System.out.printf("%s %6s \n", b, b);
		System.out.printf("%s %6s \n", b, b);
		System.out.printf("%5s \n", a);

	}

}