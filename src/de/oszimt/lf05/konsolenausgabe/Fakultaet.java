package de.oszimt.lf05.konsolenausgabe;

public class Fakultaet {

	public static void main(String[] args) {

		String a = "0!    =                    =  1";
		String b = "1!    = 1                  =  1";
		String c = "2!    = 1 * 2              =  2";
		String d = "3!    = 1 * 2 * 3          =  6";
		String e = "4!    = 1 * 2 * 3 * 4      =  24";
		String f = "5!    = 1 * 2 * 3 * 4 * 5  =  120";

		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
		System.out.println(f);

	}
}