package de.oszimt.lf05.arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class Palindrom {

	static ArrayList<String> palindrome = new ArrayList<String>();

	static void palindrome(Scanner keyboard) {
		System.out.println("Gib deine 5 Zeichen ein:");
		String s = keyboard.next();

		for (int i = 0; i < 5; i++) {
			if (s.chars().count() != 5) {
				System.out.println("Das sind keine 5 Zeichen!");
				return;
			} else {
				palindrome.add(String.valueOf(s.charAt(i)));
			}
		}

		for (int i = palindrome.size() - 1; i >= 0; i--) {
			System.out.print(palindrome.get(i));
		}
	}
}