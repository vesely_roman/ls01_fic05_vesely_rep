package de.oszimt.lf05.arrays;

public class UngeradeZahlen {

	public static void oddnumber() {
		int[] odd = new int[10];
		int count = 1;

		for (int index = 0; index < odd.length; index++) {
			odd[index] = count;
			count += 2;
		}
		for (int number : odd) {
			System.out.println(number);
		}
	}
}
