package de.oszimt.lf05.arrays;

import java.util.Arrays;

public class Lotto {

	static Integer[] numbers = { 3, 7, 12, 18, 37, 42, 14 };
	static Integer[] searchedInt = { 12, 13 };

	static void lotto() {

		System.out.print("[  ");
		for (int i : numbers) {
			System.out.print(i + "  ");
		}

		System.out.print("]");
		System.out.println("\n");

		for (int i : searchedInt) {
			boolean match = Arrays.stream(numbers).anyMatch(x -> x == i);
			if (match) {
				System.out.println("Die Zahl " + i + " ist in der Ziehung enthalten.");
			} else {
				System.out.println("Die Zahl " + i + " ist nicht in der Ziehung enthalten.");
			}
		}
	}
}