package de.oszimt.lf05.arrays;

public class ArrayHelper {

	// Aufgabe 1
	public static String convertArrayToString(int[] zahlen) {

		String returnValue = "";
		for (int zahl : zahlen) {
			returnValue += zahl + ", ";
		}
		returnValue = returnValue.substring(0, returnValue.length() - 2);

		return returnValue;
	}

	// Aufgabe 2
	public static int[] reverseArray(int[] arrayInput) {
		int[] array = arrayInput;
		for (int i = 0; i < array.length / 2; i++) {
			int temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp;
		}
		return array;
	}

	// Aufgabe 3
	public static int[] reverseArray2(int[] arrayInput) {
		int[] array = new int[arrayInput.length];
		for (int i = arrayInput.length - 1; i >= 0; i--) {
			array[i] = arrayInput[arrayInput.length - i - 1];
		}
		return array;
	}

	// Aufgabe 4
	public static double[][] tempTable(int rows) {
		double[][] table = new double[rows][2];
		for (int i = 0; i < rows; i++) {
			table[i][0] = i * 10;
			table[i][1] = (5.0 / 9.0) * ((i * 10) - 32);
		}
		return table;
	}

	// Aufgabe 5
	public static int[][] matrix(int n, int m) {
		return new int[n][m];
	}

	// Aufgabe 6
	public static boolean isMatrixTrans(int[][] matrix) {
		int rowLength = matrix.length;
		for (int[] column : matrix) {
			if (!(column.length == rowLength)) {
				return false;
			}
		}
		return true;
	}
}