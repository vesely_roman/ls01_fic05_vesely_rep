package de.oszimt.lf05.raumschiffe;

public class Main {
	public static void main(String[] args) {

		Spaceship enterprise = new Spaceship("Enterprise", 2, 100, 100, 100, 100, 3);
		Spaceship borg = new Spaceship("Borg", 10, 100, 100, 100, 100, 3);

		Cargo cargoKlingons = new Cargo("Klingons:", 200);
		Cargo cargoRomulans = new Cargo("Romulans:", 300);
		Cargo cargoVulcans = new Cargo("Vulcans:", 50);
		Cargo cargoTricorders = new Cargo("Tricorders:", 120);
		Cargo cargoResearchProbe = new Cargo("Research Probe:", 200);

		borg.addCargo(cargoKlingons);
		borg.addCargo(cargoRomulans);
		borg.addCargo(cargoVulcans);
		enterprise.addCargo(cargoTricorders);
		enterprise.addCargo(cargoResearchProbe);

		borg.broadcastAll("We are the Borg. Lower your shields and surrender your ships.");
		System.out.println(Spaceship.logsReturn());
		Spaceship.logsClear();

		enterprise.broadcastAll("Red alert! All hands to battle stations!");
		System.out.println(Spaceship.logsReturn());
		Spaceship.logsClear();

		enterprise.launchTorpedo(borg);
		enterprise.launchPhaser(borg);
		enterprise.launchPhaser(borg);
		System.out.println(Spaceship.logsReturn());
		Spaceship.logsClear();

	}
}
