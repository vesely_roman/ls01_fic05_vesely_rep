package de.oszimt.lf05.raumschiffe;

import java.util.ArrayList;

/**
 * Klasse f�r Raumschiffe
 */
public class Spaceship {

	/**
	 * Schiffname.
	 */
	private String name;

	/**
	 * Anzahl: Photonentorpedos.
	 */
	private int torpedo;
	/**
	 * Level: Energie.
	 */
	private int energy;
	/**
	 * Level: Schilde.
	 */
	private int shields;
	/**
	 * Level: H�lle.
	 */
	private int hull;
	/**
	 * Level: Lebenserhaltungssysteme.
	 */
	private int lss;
	/**
	 * Anzahl: Androiden.
	 */
	private int droids;

	/**
	 * Array f�r Nachrichtenausgabe.
	 */
	public static ArrayList<String> broadcastComm = new ArrayList<String>();
	/**
	 * Array f�r Ladungen.
	 */
	public ArrayList<Cargo> cargoManifest = new ArrayList<Cargo>();

	/**
	 * Raumschiff-Konstruktor.
	 */
	public Spaceship() {
	}

	/**
	 * Raumschiff-Konstruktor mit allen Parametern.
	 */
	public Spaceship(String name, int torpedo, int energy, int shields, int hull, int lss, int droids) {
		this.torpedo = torpedo;
		this.energy = energy;
		this.shields = shields;
		this.hull = hull;
		this.lss = lss;
		this.droids = droids;
		this.name = name;
	}

	/**
	 * Aktueller Wert der Variable torpedo.
	 */
	public int getTorpedo() {
		return torpedo;
	}

	/**
	 * Setzt einen Wert f�r die Variable torpedo.
	 */
	public void setTorpedo(int loadTorpedo) {
		torpedo = loadTorpedo;
	}

	/**
	 * Aktueller Wert der Variable energy.
	 */
	public int getEnergy() {
		return energy;
	}

	/**
	 * Setzt einen Wert f�r die Variable energy.
	 */
	public void setEnergy(int energyNew) {
		energy = energyNew;
	}

	/**
	 * Aktueller Wert der Variable shields.
	 */
	public int getShields() {
		return shields;
	}

	/**
	 * Setzt einen Wert f�r die Variable shields.
	 */
	public void setshields(int shieldsNew) {
		shields = shieldsNew;
	}

	/**
	 * Aktueller Wert der Variable hull.
	 */
	public int getHull() {
		return hull;
	}

	/**
	 * Setzt einen Wert f�r die Variable hull.
	 */
	public void sethull(int hullNew) {
		hull = hullNew;
	}

	/**
	 * Aktueller Wert der Variable lss.
	 */
	public int getLss() {
		return lss;
	}

	/**
	 * Setzt einen Wert f�r die Variable lss.
	 */
	public void setlss(int lssNew) {
		lss = lssNew;
	}

	/**
	 * Aktueller Wert der Variable name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt einen Wert f�r die Variable name.
	 */
	public void setname(String nameNew) {
		name = nameNew;
	}

	/**
	 * Aktueller Wert der Variable droids.
	 */
	public int getDroids() {
		return droids;
	}

	/**
	 * Setzt einen Wert f�r die Variable droids.
	 */
	public void setdroids(int droids) {
		this.droids = droids;

	}

	/**
	 * Methode, um eine Ladung zum Ladungs-Array hinzuzuf�gen.
	 */
	public void addCargo(Cargo loadCargo) {
		cargoManifest.add(loadCargo);
	}

	/**
	 * Diese Methode legt fest, welches Raumschiff einen Photonentorpedo abschie�en
	 * wird. Wenn kein Torpedo vorhanden ist passiert nichts. Wenn ein Torpedo
	 * vorhanden ist, wird ein Torpedo abgefeuert und die Anzahl der vorhandenen
	 * Torpedos um 1 verringert. Es wird per Methode eine Nachricht and alle
	 * gesendet und ein Treffer am Ziel-Schiff vermerkt.
	 */
	public void launchTorpedo(Spaceship spaceship) {
		if (torpedo == 0)
			;
		else {
			torpedo--;
			broadcastAll("Fire!");
			hit(spaceship);
		}
	}

	/**
	 * Diese Methode legt fest, welches Raumschiff seine Phaserkanone abschie�en
	 * wird. Wenn nicht genug Energie vorhanden ist passiert nichts. Wenn die
	 * Energie �ber 50 ist, wird die Kanone abgefeuert und das Energielevel um 50
	 * verringert. Es wird per Methode eine Nachricht and alle gesendet und ein
	 * Treffer am Ziel-Schiff vermerkt.
	 */
	public void launchPhaser(Spaceship spaceship) {
		if (energy < 50)
			;
		else {
			energy -= 50;
			broadcastAll("Fire!");
			hit(spaceship);
		}
	}

	/**
	 * Diese Methode legt fest, welches Raumschiff von einem Schuss getroffen wird.
	 * Dabei werden die Schilde um 50 verringert. Sobald die Schilde auf 0 sind,
	 * wird die H�lle und das Energielevel um 50 verringert. Wenn die H�lle auf 0
	 * f�llt, schalten sich die Lebenserhaltungssysteme des Schiffs ab und eine
	 * Nachricht an alle wird per Methode aufgerufen.
	 */
	private void hit(Spaceship spaceship) {
		spaceship.shields -= 50;
		if (spaceship.shields <= 0) {
			spaceship.hull -= 50;
			spaceship.energy -= 50;
		}

		if (spaceship.hull <= 0) {
			spaceship.lss = 0;
			spaceship.broadcastAll("Life support systems offline!");
		}
	}

	/**
	 * Methode f�r Nachrichten an alle.
	 */
	public void broadcastAll(String message) {
		broadcastComm.add(this.name + ": " + message);
	}

	/**
	 * Die Logs werden per Arraylist ausgegeben.
	 */
	public static ArrayList<String> logsReturn() {
		return broadcastComm;
	}

	/**
	 * Die bisherigen Logs in der Arraylist werden gel�scht.
	 */
	public static ArrayList<String> logsClear() {
		return broadcastComm = new ArrayList<String>();
	}

	/**
	 * Gibt den Zustand des jeweiligen Schiffes aus.
	 */
	public void status() {
		System.out.println("\n" + this.getName() + " (Status):");
		System.out.println("Torpedos: " + this.getTorpedo() + "\nEnergy: " + this.getEnergy() + "\nShields: "
				+ this.getShields() + "\nHull: " + this.getHull() + "\nLife support systems: " + this.getLss()
				+ "\nDroids: " + this.getDroids() + "\n");
	}

	/**
	 * Methode, um die Ladungen des jeweiligen Schiffes auszugeben.
	 */
	public void cargoManifestOutput() {
		System.out.println(this.name + " (Status):");
		for (Cargo temp : cargoManifest) {
			System.out.println(temp.getType() + " " + temp.getAmount());
		}
	}
}
