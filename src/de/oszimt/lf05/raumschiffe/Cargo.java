package de.oszimt.lf05.raumschiffe;

/**
 * Klasse f�r Ladungen
 */
public class Cargo {

	/**
	 * Ladungstyp
	 */
	private String type;

	/**
	 * Anzahl des jeweiligen Ladungstyps
	 */
	private int amount;

	/**
	 * Ladungen-Konstruktor
	 */
	public Cargo() {

	}

	/**
	 * Ladungen-Konstruktor mit allen Parametern.
	 */
	public Cargo(String type, int amount) {

		this.type = type;
		this.amount = amount;
	}

	/**
	 * Aktueller Wert der Variable type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Setzt einen Wert f�r die Variable type.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Aktueller Wert der Variable amount.
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * Setzt einen Wert f�r die Variable amount.
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
